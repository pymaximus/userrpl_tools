#!/usr/bin/env python3
#
# Copyright (C) Frank Singleton b17flyboy@gmail.com
#
# Simple UserRPL code formatter. Called from emacs
#
# For now '\<<' triggers indent increase, and '\>>' triggers indent decrease
# All lines are stripped of excess whitespace via strip() also
#

import sys


def do_main():
    """Main function"""

    # defines local indent style
    indent = 0
    indent_inc = 4
    indent_string = ' '

    for line in sys.stdin:
        line = line.strip()
        if line.startswith(r'\<<'):
            # print line then increase indent
            print('{}{}'.format(indent_string * indent, line))
            indent += indent_inc
        elif line.startswith(r'\>>'):
            # decrease indent then print line
            indent -= indent_inc
            print('{}{}'.format(indent_string * indent, line))
        else:
            # print at current indent
            print('{}{}'.format(indent_string * indent, line))


if __name__ == '__main__':
    do_main()
