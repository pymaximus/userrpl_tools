;;; fs-userrpl-tools.el  --- Some tools to make UserRPL programming a better experience

;;
;; Copyright (C) 2019 Frank Singleton
;;
;; Author: Frank Singleton <b17flyboy@gmail.com>
;; Version: 0.1.1
;; Keywords: programming, sysrpl
;; URL: https://bitbucket.org/pymaximus/userrpl_tools
;; Package-Requires: ((emacs "24"))

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;
;; Provides some UserRPL tools that can be useful for programming for HP 50G
;;

;;; Code:

(defgroup fs-userrpl nil
  "Tools for UserRPL programming."
  :prefix "fs-userrpl-"
  :group 'applications)

(defcustom fs-userrpl-formatter-program "/home/frank/repos/userrpl_tools/userrpl_format.py"
  "External program that formats UserRPL code."
  :type '(string)
  :group 'fs-userrpl)

(defun fs-userrpl-format-on-buffer ()
  "Format the current buffer using UserRPL formatting."
  (interactive)
  (save-excursion
    (shell-command-on-region (point-min) (point-max) fs-userrpl-formatter-program t t)
    (whitespace-cleanup)
    ))

(provide 'fs-userrpl-tools)
;;; fs-userrpl-tools.el ends here
