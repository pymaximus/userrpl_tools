userrpl_tools
============

Some tools to make writing UserRPL a little easier inside Emacs.

fs-userrpl-tools.el
-------------------

Provides tools for formatting UserRPL code. 


userrpl_format.py
----------------

The external code formatter called from **fs-userrpl-tools**

Initially, indents based off **\<<** and **\\>>** . Needs work but this is a start.


Inside Emacs, use **load-file** to load **fs-userrpl-tools.el** and then issue the following
command in the buffer containing UserRPL code.

``` text
M-x fs-userrpl-format-on-buffer
```

This should format/modify the buffer in place.

See **customize-group fs-userrpl** for more details.


NOTE: There is a small UserRPL program **settime.rpl** that you can test 
these tools against. 

Just modify the indentation then execute **fs-userrpl-tools.el** on the buffer.

Cheers ..
